import React, { Component } from 'react'
import { DATAGLASSES } from './dataGlasses'

export default class Body extends Component {
    state = {
        chooseGlase: "./glassesImage/v1.png",
        name: "GUCCI G8850U",
        price: 30,
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
    }
    handleOnclickGlasse = (item) => {
        let { url, name, price, desc } = item
        this.setState({
            chooseGlase: url,
            name: name,
            price: price,
            desc: desc
        })
    }
    renderGlasses = () => {
        return DATAGLASSES.map((glasse) => {
            console.log(glasse.url)
            return (
                <div className='col-2'>
                    <img onClick={() => { this.handleOnclickGlasse(glasse) }} style={{ width: 150 }} src={glasse.url} />
                </div>
            )
        })
    }
    render() {
        return (
            <div style={{}} className='bg-secondary'>
                <div className="row  justify-content-between">
                    <div className='col-5'>
                        <img src="./glassesImage/model.jpg" alt="" />
                        <img style={{
                            top: '25%',
                            left: '35.5%',
                            width: '230px',
                            opacity: '0.8'
                        }} className="position-absolute" src={this.state.chooseGlase} alt="" />
                    </div>
                    <div style={{
                        top: '49.5%',
                        left: '7.8%',
                        width: '482px',
                        height: '180px',
                        opacity: '0.4'
                    }} className="position-absolute bg-dark">
                        <div className='text-white p-4 text-left'>
                            <p>Name: {this.state.name}</p>
                            <p>Price: {this.state.price}$</p>
                            <p>Desc: {this.state.desc}</p>
                        </div>
                    </div>
                    <div className='col-5'><img src="./glassesImage/model.jpg" alt="" /></div>

                </div>
                <div className='container'>
                    <div className='mt-5 p-5 row bg-white'>
                        {this.renderGlasses()}
                    </div>
                </div>
            </div>
        )
    }

}
